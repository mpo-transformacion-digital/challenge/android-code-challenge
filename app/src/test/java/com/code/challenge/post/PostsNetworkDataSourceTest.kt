package com.code.challenge.post

import com.code.challenge.data.remote.api.TypicodeRemoteAPI
import com.code.challenge.data.remote.model.PaginatedResponse
import com.code.challenge.data.remote.model.Welcome3Element
import com.code.challenge.data.remote.source.TypicodeRemoteDS
import com.code.challenge.util.defaultConverter
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.ExperimentalSerializationApi
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.*
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@ExperimentalCoroutinesApi
@ExperimentalSerializationApi
class PostsNetworkDataSourceTest {

    private val mockWebServer = MockWebServer()

    private val client = OkHttpClient.Builder()
        .connectTimeout(1, TimeUnit.SECONDS)
        .readTimeout(1, TimeUnit.SECONDS)
        .writeTimeout(1, TimeUnit.SECONDS)
        .build()

    private val api = Retrofit.Builder()
        .baseUrl(mockWebServer.url("http://jsonplaceholder.typicode.com/"))
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(TypicodeRemoteAPI::class.java)

    private val sut = TypicodeRemoteDS(api)

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `Obtener post del servicio Rtrofit`() {
        var mockResponse = MockResponse()
        mockResponse.setResponseCode(200)
        mockResponse.setBody("{\n" +
                "    \"userId\": 1,\n" +
                "    \"id\": 1,\n" +
                "    \"title\": \"sunt aut facere repellat provident occaecati excepturi optio reprehenderit\",\n" +
                "    \"body\": \"quia et suscipit\\nsuscipit recusandae consequuntur expedita et cum\\nreprehenderit molestiae ut ut quas totam\\nnostrum rerum est autem sunt rem eveniet architecto\"\n" +
                "  }")
        mockWebServer.enqueue(mockResponse)

        runBlocking {
            val actual = sut.getAllPost()

            assertNotNull(actual)
        }
    }

    @Test
    fun `Valida resultado con objeto y Retrofit`() {
        var mockResponse = MockResponse()
        mockResponse.setResponseCode(200)
        mockResponse.setBody("{\n" +
                "    \"userId\": 1,\n" +
                "    \"id\": 1,\n" +
                "    \"title\": \"sunt aut facere repellat provident occaecati excepturi optio reprehenderit\",\n" +
                "    \"body\": \"quia et suscipit\\nsuscipit recusandae consequuntur expedita et cum\\nreprehenderit molestiae ut ut quas totam\\nnostrum rerum est autem sunt rem eveniet architecto\"\n" +
                "  }")
        mockWebServer.enqueue(mockResponse)

        runBlocking {
            val actual = sut.getAllPost()

            val expected = listOf(
                Welcome3Element(
                    1,
                    id = 1,
                    title = "Wonder Woman 1984",
                    "3"
                )
            ).toPaginatedResponse()

            assertNotEquals(expected, actual)
        }
    }

}

internal fun List<Any>.toPaginatedResponse(page: Int = 1) = PaginatedResponse(page, this)