package com.code.challenge.post

import com.code.challenge.data.remote.model.Welcome3Element

internal fun welcome3Element(
    userID : Long = 0,
    id: Long = 0,
    title : String = "",
    body : String =  "String"
) = Welcome3Element(
    userID,
    id,
    title,
    body
)