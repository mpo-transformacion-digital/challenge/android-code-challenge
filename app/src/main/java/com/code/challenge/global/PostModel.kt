package com.code.challenge.global

import java.io.Serializable

class PostModel(
    var userID: Long,
    var id: Long,
    var title: String,
    var body: String) : Serializable