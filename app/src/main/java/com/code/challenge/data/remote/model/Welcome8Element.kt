package com.code.challenge.data.remote.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class Welcome8Element (
    @SerialName("postId")
    val postID: Long,

    val id: Long,
    val name: String,
    val email: String,
    val body: String
)

