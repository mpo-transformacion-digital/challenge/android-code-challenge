package com.code.challenge.data.remote.api

import com.code.challenge.data.remote.model.Welcome3Element
import com.code.challenge.data.remote.model.Welcome8Element
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface TypicodeRemoteAPI {

    @GET("posts")
    fun getAllPost() : Call<List<Welcome3Element>>

    @GET("posts/{postid}/comments")
    fun getAllCommentsByIdPost(@Path("postid") postId : Long) : Call<List<Welcome8Element>>
}