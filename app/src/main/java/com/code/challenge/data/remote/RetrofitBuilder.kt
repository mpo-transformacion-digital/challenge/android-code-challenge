package com.code.challenge.data.remote

import androidx.viewbinding.BuildConfig
import com.code.challenge.data.remote.api.TypicodeRemoteAPI
import com.code.challenge.util.defaultConverter
import kotlinx.serialization.ExperimentalSerializationApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@OptIn(ExperimentalSerializationApi::class)
object RetrofitBuilder {

    private const val BASE_URL = "https://jsonplaceholder.typicode.com/"

    @ExperimentalSerializationApi
    private fun getRetrofit() : Retrofit {

        val retrofitBuilder = Retrofit.Builder()
            .baseUrl(BASE_URL)
            //.addConverterFactory(defaultConverter(isLenient = true))
            .addConverterFactory(GsonConverterFactory.create())

        if (BuildConfig.BUILD_TYPE == "debug") {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
            retrofitBuilder.client(client)
        }

        return retrofitBuilder.build()
    }

    val typicodeRemoteAPI: TypicodeRemoteAPI = getRetrofit().create(TypicodeRemoteAPI::class.java)

}