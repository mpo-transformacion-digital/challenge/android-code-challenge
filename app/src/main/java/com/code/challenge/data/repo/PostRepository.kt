package com.code.challenge.data.repo

import com.code.challenge.data.remote.model.Welcome3Element

interface PostRepository {

    suspend fun getAllPost() : List<Welcome3Element>?
}