package com.code.challenge.data.remote.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Welcome3Element (
    @SerializedName("userId")
    val userID: Long,

    val id: Long,
    val title: String,
    val body: String
) : Serializable