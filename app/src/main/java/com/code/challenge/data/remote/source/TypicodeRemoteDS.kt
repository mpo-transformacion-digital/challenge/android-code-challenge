package com.code.challenge.data.remote.source

import com.code.challenge.data.remote.api.TypicodeRemoteAPI
import com.code.challenge.data.remote.model.Welcome3Element
import com.code.challenge.data.remote.model.Welcome8Element
import com.code.challenge.data.repo.CommentsRepository
import com.code.challenge.data.repo.PostRepository
import retrofit2.await

class TypicodeRemoteDS (private val typicodeRemoteAPI: TypicodeRemoteAPI) : PostRepository, CommentsRepository{

    override suspend fun getAllPost(): List<Welcome3Element>? {
        return typicodeRemoteAPI.getAllPost().await()
    }

    override suspend fun getAllCommentsByPostId(idPost: Long): List<Welcome8Element> {
        return typicodeRemoteAPI.getAllCommentsByIdPost(idPost).await()
    }

}