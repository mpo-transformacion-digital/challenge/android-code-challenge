package com.code.challenge.data.repo

import com.code.challenge.data.remote.model.Welcome8Element

interface CommentsRepository {

    suspend fun getAllCommentsByPostId(idPost : Long) : List<Welcome8Element>?
}