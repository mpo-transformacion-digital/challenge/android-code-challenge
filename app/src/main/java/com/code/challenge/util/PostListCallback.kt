package com.code.challenge.util

import com.code.challenge.data.remote.model.Welcome3Element

interface PostListCallback {
    fun onClick(post: Welcome3Element)
}