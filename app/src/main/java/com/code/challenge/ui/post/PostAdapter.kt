package com.code.challenge.ui.post

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.code.challenge.data.remote.model.Welcome3Element
import com.code.challenge.databinding.ItemPostBinding
import com.code.challenge.util.PostListCallback

class PostAdapter(
    private var post: List<Welcome3Element>,
    private val callBack: PostListCallback) :
    RecyclerView.Adapter<PostAdapter.PostViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val binding = ItemPostBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PostViewHolder(binding,callBack)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        val currentPost = post[position]
        holder.binding.post = currentPost
        holder.bind(currentPost)
    }

    override fun getItemCount(): Int = post.size

    class PostViewHolder(
        val binding: ItemPostBinding,
        private val callBack: PostListCallback) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item : Welcome3Element){}
        init {
            binding.root.setOnClickListener {
                binding.post?.let { post ->
                    callBack.onClick(post)
                }
            }
        }
    }

}