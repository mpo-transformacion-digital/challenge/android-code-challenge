package com.code.challenge.ui.post

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.ScrollView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.RecyclerView
import com.code.challenge.R
import com.code.challenge.data.remote.model.Welcome3Element
import com.code.challenge.global.PostModel
import com.code.challenge.util.PostListCallback
import com.google.gson.Gson
import kotlinx.coroutines.launch

class PostFragment : Fragment(), PostListCallback {

    companion object {
        fun newInstance() = PostFragment()
    }

    private lateinit var postList: RecyclerView
    private lateinit var viewModel: PostViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.post_fragment, container, false)
        postList = rootView.findViewById(R.id.randome_list)

        val postFactory = PostViewModel.PostViewModelFactory()
        viewModel = ViewModelProvider(this, postFactory)[PostViewModel::class.java]

        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        this.view?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.VISIBLE

        viewModel = ViewModelProvider(this)[PostViewModel::class.java]
        viewModel.post.observe(viewLifecycleOwner) {
            postList.adapter = viewModel.post.value?.let {
                PostAdapter(it,this)
            }
            this.view?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE
            this.view?.findViewById<ScrollView>(R.id.scroll_discovery)?.visibility = View.VISIBLE
            }

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.getAllPost()
        }

    }

    override fun onClick(post: Welcome3Element) {
        val directions = PostFragmentDirections.actionPostFragmentToPostDetailFragment(post)
        directions.arguments.putSerializable("post_arg", post)
        NavHostFragment.findNavController(this).navigate(directions)
    }

}