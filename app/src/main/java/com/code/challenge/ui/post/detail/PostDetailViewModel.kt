package com.code.challenge.ui.post.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.code.challenge.data.remote.RetrofitBuilder
import com.code.challenge.data.remote.model.Welcome8Element
import com.code.challenge.data.remote.source.TypicodeRemoteDS
import com.code.challenge.data.repo.CommentsRepository
import com.code.challenge.ui.post.PostViewModel
import java.lang.Exception

class PostDetailViewModel(private val commentsRepository: CommentsRepository) : ViewModel() {

    var commentsGlobalModel = MutableLiveData<List<Welcome8Element>?>()

    suspend fun searchComments(userId : Long) {
        var comments = commentsRepository.getAllCommentsByPostId(userId)
        if (comments != null) {
            commentsGlobalModel.value = comments
        }
    }

    class PostCommentsDetailViewModelFactory: ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(PostDetailViewModel::class.java)){
                return PostDetailViewModel(
                    commentsRepository = TypicodeRemoteDS(RetrofitBuilder.typicodeRemoteAPI),
                ) as T
            }
            throw Exception("No class type supported")
        }
    }

}