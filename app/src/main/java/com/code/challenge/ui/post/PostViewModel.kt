package com.code.challenge.ui.post

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.code.challenge.data.remote.RetrofitBuilder
import com.code.challenge.data.remote.model.Welcome3Element
import com.code.challenge.data.remote.source.TypicodeRemoteDS
import com.code.challenge.data.repo.PostRepository
import com.code.challenge.util.ObservableViewModel
import mpo.pokemon.util.Resource
import java.lang.Exception

class PostViewModel(private val postRepository : PostRepository) : ObservableViewModel() {

    val post = MutableLiveData<List<Welcome3Element>?>()

    suspend fun getAllPost(){
        try {
            post.value = postRepository.getAllPost()
        }
        catch (ex : Exception) {
            Resource.error(null, ex.localizedMessage)
        }
    }

    class PostViewModelFactory: ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(PostViewModel::class.java)){
                return PostViewModel(
                    postRepository = TypicodeRemoteDS(RetrofitBuilder.typicodeRemoteAPI),
                ) as T
            }
            throw Exception("No class type supported")
        }
    }

}