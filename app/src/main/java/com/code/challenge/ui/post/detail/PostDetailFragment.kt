package com.code.challenge.ui.post.detail

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import com.code.challenge.R
import com.code.challenge.data.remote.model.Welcome3Element
import com.code.challenge.databinding.PostDetailFragmentBinding
import com.code.challenge.ui.post.PostViewModel
import com.google.gson.Gson
import kotlinx.coroutines.launch

class PostDetailFragment : DialogFragment() {

    private lateinit var viewModel: PostDetailViewModel
    private lateinit var binding: PostDetailFragmentBinding
    private lateinit var post : Welcome3Element

    companion object {
        fun newInstance() = PostDetailFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        post = arguments?.getSerializable("post_arg") as Welcome3Element
        binding = PostDetailFragmentBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            postDetail = post
        }

        val postDetailFactory = PostDetailViewModel.PostCommentsDetailViewModelFactory()
        viewModel = ViewModelProvider(this, postDetailFactory)[PostDetailViewModel::class.java]

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(PostDetailViewModel::class.java)
        // TODO: Use the ViewModel
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.searchComments(post.id)
            binding.postTitle.text = post.title
            binding.postBody.text = post.body
            binding.numComments.text = viewModel.commentsGlobalModel.value!!.size.toString()
        }
    }
}